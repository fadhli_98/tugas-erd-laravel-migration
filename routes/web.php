<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home'); 
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');
Route::get('/data-tables','IndexController@tables');

//CRUD cast

//create
Route::get('/cast','CastController@index');//ambil data ke data ditampilkan di blade

Route::post('/cast','CastController@store'); //simpan data form ke data base


//read
Route::get('/cast/create','CastController@create'); //mengarah ke form tambah data pemain

Route::get('/cast/{cast_id}','CastController@show'); //reroute detail kategori

Route::put('/cast/{cast_id}','CastController@update');


//edit
Route::get('/cast/{cast_id}/edit','CastController@edit'); //route untuk edit

Route::put('/cast/{cast_id}','CastController@update');//route untuk update

//delete
Route::delete('/cast/{cast_id}','CastController@destroy');//delete data berdasarkan ID