@extends('layout.master')
@section('title')
Halaman Form
@endsection
@section('content')
<h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
    @csrf
        <label>First Name</label> <br> <br>
        <input type="text" name="first_name"> <br> <br>
        <label>Last Name</label> <br> <br>
        <input type="text" name="last_name"> <br> <br>
        <label>Gender</label> <br> <br>
        <input type="radio" value="132" name="kl"> Male <br>
        <input type="radio" value="134" name="kl"> Female <br> <br>
        <label>Nationality</label> <br> <br>
        <select name="Nationality" id="">
            <option value="11">Indonesia</option>
            <option value="12"> English</option>
            <option value="13">Rusia</option>
        </select> <br> <br> <br>
        <label>Language Spoken</label> <br> <br>
        <input type="checkbox" value="1" name="Language Spoken"> Bahasa Indonesia <br>
        <input type="checkbox" value="2" name="Language Spoken"> English <br>
        <input type="checkbox" value="3" name="Language Spoken"> other <br> <br> <br>
        <label>Bio</label> <br> <br>
        <textarea name="Bio" cols="40" rows="20"></textarea> <br> <br> <br>
        <input type="submit" value="Sign Up"><br><br><br><br>
        




        <a href='/'>Kembali ke Halaman Utama</a>
    </form>
@endsection