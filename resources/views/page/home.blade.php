@extends('layout.master')
@section('title')
Halaman Home
@endsection
@section('content')
<h1>Media Online</h1>
    <h3>Sosial Media Developer</h3>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h3>Benefit Join di Media Online</h3>
<ul>
     <li>Mendapat Motivasi dari sesama para Developer</li>
     <li>Sharing Knowledge</li>
     <li>Dibuat oleh calon Web Developer terbaik</li>
 </ul>
<h3>Cara Bergabung ke Media Online</h3>
 <ol>
     <li>Mengunjungi Website ini</li>
     <li>Mendaftarkan di <a href='/register'>Form Sign Up</a></li>
     <li>Selesai</li>
@endsection